# FS1000-Javascript-Review

Instructions for NPM

1) Run NPM init in your terminal
2) It will ask you several questions (ie: project name, copyright etc.); where it asks you your project entry point, change index.js to app.js
3) Go to NPM registry online and find express. Find the install instructions (hint: (npm install -- save express) ) for express, and paste into terminal. Run the install and express will show up under dependencies in package.json file.
4) Now you have access to the Express module for use in your project! 

Definitions:

Node - A javascript runtime environment

REPL - Read-Evaluate-Print-Loop

Node module -  A module is essentially a separate file with a grouping of related functionality that we can export and import to use with other modules. 

Dependencies - Dependencies (which can be found in the package.json file with NPM) any module that another module requires in order to function. Ex: express in our App.js file. 

HTTP request -  An HTTP request is made by a client, to a host, which is located on a server. The aim of the request is to access a resource on the server.
